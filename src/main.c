#include "hardware.h"


#define FILTER_ACCURACITY 6
#define FILTER_DEGREE 4
#define FILTER_COEF 1

//static_assert((1 << FILTER_DEGREE) > FILTER_COEF, "wrong filter configs");

uint16_t SuperFilter(uint16_t rawVal)
{
    static uint32_t filtVal = 0;
    uint32_t rawAccurVal = rawVal << FILTER_ACCURACITY;

    filtVal = ( filtVal * ((1 << FILTER_DEGREE) - FILTER_COEF) + rawAccurVal * FILTER_COEF ) >> FILTER_DEGREE;

    return filtVal >> FILTER_ACCURACITY;
}


int main(void)
{
	HARDWARE_ClockInit();
	HARDWARE_SysTickInit(1);
	GPIO_Init();
	UART_Init(MDR_UART2, 921600);
	ADC_Init(4);
	
	uint32_t oldTick = HARDWARE_GetTick();
	const uint32_t delayTick = 10;

	while(1)
	{
		if(oldTick + delayTick <= HARDWARE_GetTick())
		{
			oldTick = HARDWARE_GetTick();
			GPIO_TogglePin(LED3_PORT, LED3_PIN_Pos);
			uint16_t adcVal = ADC_Get(6);
			uint16_t voltage_mV = (3300 * (uint32_t)adcVal) / 4096;
			uint16_t current_mA = ((100 * voltage_mV) / 50) / 2 + 36;
			uint16_t currentFilt_mA = SuperFilter(current_mA);

			UART_Printf(MDR_UART2, "\r\n%u %u\3", current_mA, currentFilt_mA);
		}
	}
}

